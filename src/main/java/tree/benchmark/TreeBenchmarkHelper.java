package tree.benchmark;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import tree.Tree;
import tree.TreeRepo;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class TreeBenchmarkHelper
{
    private static String getJsonData(String filename)
    {
        try {
            InputStream stream = Tree.class.getClassLoader().getResourceAsStream(filename);
            ByteArrayOutputStream result = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;
            while ((length = stream.read(buffer)) != -1) {
                result.write(buffer, 0, length);
            }
            return result.toString("UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static TreeRepo buildRepo(String filename) throws JSONException
    {
        String jsonData = TreeBenchmarkHelper.getJsonData(filename);

        JSONArray jsonArray = new JSONArray(jsonData);
        TreeRepo repo = new TreeRepo();

        for(int i = 0; i < jsonArray.length(); i++){
            JSONObject object = jsonArray.getJSONObject(i);
            repo.addElement(object.getInt("unique_id"), object.getInt("parent_id"));
        }

        return repo;
    }
}
