package tree;

import java.util.ArrayList;
import java.util.HashMap;

public class TreeRepo
{
    private ArrayList<HashMap<String, Integer>> elements;

    public TreeRepo()
    {
        this.elements = new ArrayList<>();
    }

    public void addElement(Integer uniqueId, Integer parentId)
    {
        HashMap<String, Integer> element = new HashMap<>();
        element.put("unique_id", uniqueId);
        element.put("parent_id", parentId);
        this.elements.add(element);
    }

    public ArrayList<HashMap<String, Integer>> getElements()
    {
        return this.elements;
    }
}
