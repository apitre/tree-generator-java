package tree.benchmark;

import org.json.JSONException;
import org.openjdk.jmh.annotations.*;
import tree.Tree;
import tree.TreeRepo;

import java.util.concurrent.TimeUnit;

public class TreeBenchmark
{
    @State(Scope.Thread)
    public static class TreeContext
    {
        public TreeRepo repo;

        @Setup(Level.Trial)
        public void doSetupBenchmark() throws JSONException
        {
            this.repo = TreeBenchmarkHelper.buildRepo("data.json");
        }

        public void doSetupTest() throws JSONException
        {
            this.repo = TreeBenchmarkHelper.buildRepo("data-test.json");
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.SECONDS)
    @Fork(value = 1, warmups = 1)
    public void buildTree(TreeContext context)
    {
        Tree tree = new Tree(context.repo);
        tree.build();
    }
}