package tree;

import java.util.*;

public class Tree implements TreeInterface
{
    private TreeRepo repo;
    private Map<Integer, TreeNode> nodes;

    public Tree(TreeRepo repo)
    {
        this.repo = repo;
        this.nodes = new HashMap<>();
    }

    private class TreeStructure
    {
        public HashMap<Integer, Integer> rows = new HashMap<>();
        public HashMap<Integer, ArrayList<Integer>> children = new HashMap();
    }

    private class TreeNode
    {
        public Integer parent;
        public List<Integer> direct;
        public List<Integer> indirect;

        public  TreeNode(Integer parent, List<Integer> direct, List<Integer> indirect)
        {
            this.direct = direct;
            this.indirect = indirect;
            this.parent = parent;
        }
    }

    public List<Integer> getAncestorIds(Integer parentId)
    {
        return this.getAncestorIds(parentId, true);
    }

    private List<Integer> addParentId(Integer parentId, List<Integer> ids, boolean includeRootId)
    {
        if (this.nodes.get(parentId).parent == 0 && includeRootId == false) {
            return ids;
        }

        ids.add(this.nodes.get(parentId).parent);

        if (this.nodes.get(parentId).parent > 0) {
            return this.addParentId(this.nodes.get(parentId).parent, ids, includeRootId);
        }

        return ids;
    }

    public List<Integer> getAncestorIds(Integer parentId, boolean includeRootId)
    {
        if (this.nodes.containsKey(parentId) == false) {
            return null;
        }

        if (parentId == 0) {
            return includeRootId ? new ArrayList<>(Arrays.asList(0)) : new ArrayList<>();
        }

        return this.addParentId(parentId, new ArrayList<>(Arrays.asList(parentId)), includeRootId);
    }

    public Integer getTopParentId(Integer id)
    {
        List<Integer> ancestorIds = this.getAncestorIds(id, false);
        if (ancestorIds != null && ancestorIds.size() > 1) {
            return ancestorIds.get(ancestorIds.size() - 1);
        }
        return null;
    }

    private List<Integer> buildFromParents(Integer i, final Integer parent, Map<Integer, Integer> rows, Map<Integer, ArrayList<Integer>> children)
    {
        List<Integer> indirect = new ArrayList<>();
        List<Integer> direct = new ArrayList<>();

        for (int z = 0; z < children.get(i).size(); z++) {

            int id = children.get(i).get(z);

            if (rows.containsKey(id)) {

                direct.add(id);

                if (children.containsKey(id)) {
                    indirect.addAll(this.buildFromParents(id, i, rows, children));
                } else {
                    this.nodes.put(id, new TreeNode(i, new ArrayList<>(), new ArrayList<>()));
                }

            }

        }

        this.nodes.put(i, new TreeNode(parent, new ArrayList<>(direct), new ArrayList<>(indirect)));

        indirect.addAll(direct);

        return indirect;
    }

    public List<Integer> getDirectChildrenIds(Integer parentId)
    {
        if (!this.nodes.containsKey(parentId)) {
            return null;
        }
        return this.nodes.get(parentId).direct;
    }

    public List<Integer> getParentAndDescendants(Integer id)
    {
        if (this.nodes.containsKey(id) == false) {
            return null;
        }

        List<Integer> parentAndDescendants = new ArrayList(this.getDirectChildrenIds(id));
        parentAndDescendants.addAll(this.getIndirectChildrenIds(id));
        parentAndDescendants.add(id);

        return parentAndDescendants;
    }

    public boolean nodeExits(Integer id)
    {
        return this.nodes.containsKey(id);
    }

    public boolean moveBranchId(Integer id, Integer parentId)
    {
        if (this.nodes.containsKey(id) == false || id == null) {
            return false;
        }

        if (id == 0) {
            this.nodes = new HashMap<>();
            return true;
        }

        List<Integer> parentAndDescendants = this.getParentAndDescendants(id);
        List<Integer> ancestorIds = this.getAncestorIds(id);

        for (int i = 0; i < ancestorIds.size(); i++) {
            if (i != 0) {
                this.nodes.get(ancestorIds.get(i)).direct.removeAll(parentAndDescendants);
                this.nodes.get(ancestorIds.get(i)).indirect.removeAll(parentAndDescendants);
            }
        }

        ancestorIds = this.getAncestorIds(id);

        for (int i = 0; i < ancestorIds.size(); i++) {
            if (i != 0) {
                this.nodes.get(ancestorIds.get(i)).direct.removeAll(parentAndDescendants);
                this.nodes.get(ancestorIds.get(i)).indirect.removeAll(parentAndDescendants);
            }
        }

        return true;
    }

    public boolean removeBranchById(Integer id)
    {
        if (this.nodes.containsKey(id) == false || id == null) {
            return false;
        }

        if (id == 0) {
            this.nodes = new HashMap<>();
            return true;
        }

        List<Integer> parentAndDescendants = this.getParentAndDescendants(id);
        List<Integer> ancestorIds = this.getAncestorIds(id);

        for (int i = 0; i < ancestorIds.size(); i++) {
            if (i == 0) {
                this.nodes.keySet().removeAll(parentAndDescendants);
            } else {
                this.nodes.get(ancestorIds.get(i)).direct.removeAll(parentAndDescendants);
                this.nodes.get(ancestorIds.get(i)).indirect.removeAll(parentAndDescendants);
            }
        }

        return true;
    }

    public boolean addChildrenId(Integer parentId, Integer id)
    {
        if (this.nodes.containsKey(parentId) == false || id == null || id == 0) {
            return false;
        }

        List<Integer> ancestorIds = this.getAncestorIds(parentId);

        for (int i = 0; i < ancestorIds.size(); i++) {
            if (i == 0) {
                this.nodes.get(ancestorIds.get(i)).direct.add(id);
            } else {
                this.nodes.get(ancestorIds.get(i)).indirect.add(id);
            }
        }

        return true;
    }

    public List<Integer> getIndirectChildrenIds(Integer parentId)
    {
        if (!this.nodes.containsKey(parentId)) {
            return null;
        }
        return this.nodes.get(parentId).indirect;
    }

    public Integer getParentId(Integer id)
    {
        if (!this.nodes.containsKey(id)) {
            return null;
        }
        return this.nodes.get(id).parent;
    }

    public void build()
    {
        ArrayList<HashMap<String, Integer>> elements = this.repo.getElements();

        this.nodes = new HashMap<>();

        TreeStructure structure = this.getParentChildrenStructure(elements);

        this.buildFromParents(0, null, structure.rows, structure.children);
    }

    private TreeStructure getParentChildrenStructure(ArrayList<HashMap<String, Integer>> elements)
    {
        TreeStructure structure = new TreeStructure();

        for (HashMap<String, Integer> element : elements) {

            Integer p = element.get("parent_id");
            Integer u = element.get("unique_id");

            if (!structure.children.containsKey(p)) {
                structure.children.put(p, new ArrayList<Integer>());
            }

            structure.children.get(p).add(u);
            structure.rows.put(u, p);

        }

        return structure;
    }
}
