package tree;

import org.json.JSONException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import tree.benchmark.TreeBenchmark;

import java.util.ArrayList;
import java.util.Arrays;

public class TreeTest
{
    private static Tree tree;

    @Before
    public void setupBeforeClass() throws JSONException
    {
        TreeBenchmark.TreeContext context = new TreeBenchmark.TreeContext();
        context.doSetupTest();

        TreeTest.tree = new Tree(context.repo);
        TreeTest.tree.build();
    }

    @Test
    public void testGetParentId()
    {
        Assert.assertEquals(Integer.valueOf(111), TreeTest.tree.getParentId(1111));
        Assert.assertEquals(null, TreeTest.tree.getParentId(999999));
    }

    @Test
    public void testGetDirectChildrenIds()
    {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(11,12,13,14,15)), TreeTest.tree.getDirectChildrenIds(1));
        Assert.assertEquals(null, TreeTest.tree.getDirectChildrenIds(9999));
        Assert.assertEquals(new ArrayList<Integer>(), TreeTest.tree.getDirectChildrenIds(2));
    }

    @Test
    public void testGetIndirectChildrenIds()
    {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1111,1112,111,112,113,114,115)), TreeTest.tree.getIndirectChildrenIds(1));
        Assert.assertEquals(null, TreeTest.tree.getIndirectChildrenIds(9999));
        Assert.assertEquals(new ArrayList<Integer>(), TreeTest.tree.getIndirectChildrenIds(111));
    }

    @Test
    public void testNodeExists()
    {
        Assert.assertTrue(TreeTest.tree.nodeExits(11));
        Assert.assertTrue(TreeTest.tree.nodeExits(0));
        Assert.assertFalse(TreeTest.tree.nodeExits(9999));
        Assert.assertFalse(TreeTest.tree.nodeExits(null));
    }

    @Test
    public void testGetTopParentId()
    {
        Assert.assertEquals(Integer.valueOf(1), TreeTest.tree.getTopParentId(1111));
        Assert.assertEquals(null, TreeTest.tree.getTopParentId(99999));
        Assert.assertEquals(null, TreeTest.tree.getTopParentId(1));
    }

    @Test
    public void testGetAncerstorsIds()
    {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(55551,5551,551,51,5,0)), TreeTest.tree.getAncestorIds(55551));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(55551,5551,551,51,5,0)), TreeTest.tree.getAncestorIds(55551, true));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(0)), TreeTest.tree.getAncestorIds(0, true));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(55551,5551,551,51,5)), TreeTest.tree.getAncestorIds(55551, false));
        Assert.assertEquals(new ArrayList<>(), TreeTest.tree.getAncestorIds(0, false));
        Assert.assertEquals(null, TreeTest.tree.getAncestorIds(999999));
    }

    @Test
    public void testGetParentAndDescendants()
    {
        Assert.assertEquals(new ArrayList<>(Arrays.asList(11,12,13,14,15,1111,1112,111,112,113,114,115,1)), TreeTest.tree.getParentAndDescendants(1));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1111)), TreeTest.tree.getParentAndDescendants(1111));
        Assert.assertEquals(null, TreeTest.tree.getParentAndDescendants(99999));
    }

    @Test
    public void testAddChildrenId()
    {
        Assert.assertFalse(TreeTest.tree.addChildrenId(9999, 1113));
        Assert.assertFalse(TreeTest.tree.addChildrenId(1111, null));
        Assert.assertFalse(TreeTest.tree.addChildrenId(1111, 0));
        Assert.assertTrue(TreeTest.tree.addChildrenId(111, 1113));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1111,1112,1113)), TreeTest.tree.getDirectChildrenIds(111));
        Assert.assertEquals(new ArrayList<>(), TreeTest.tree.getIndirectChildrenIds(111));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(111,112,113,114,115)), TreeTest.tree.getDirectChildrenIds(11));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1111,1112,1113)), TreeTest.tree.getIndirectChildrenIds(11));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(11,12,13,14,15)), TreeTest.tree.getDirectChildrenIds(1));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1111,1112,111,112,113,114,115,1113)), TreeTest.tree.getIndirectChildrenIds(1));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1,2,3,4,5,1111,1112,111,112,113,114,115,11,12,13,14,15,55551,5551,551,552,51,52,53,54,55,1113,0)), TreeTest.tree.getParentAndDescendants(0));

        Assert.assertTrue(TreeTest.tree.addChildrenId(0, 6));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1,2,3,4,5,6)), TreeTest.tree.getDirectChildrenIds(0));
        Assert.assertEquals(new ArrayList<>(Arrays.asList(1,2,3,4,5,6,1111,1112,111,112,113,114,115,11,12,13,14,15,55551,5551,551,552,51,52,53,54,55,1113,0)), TreeTest.tree.getParentAndDescendants(0));
    }

    @Test
    public void testRemoveBranchByIdFail()
    {
        Assert.assertFalse(TreeTest.tree.removeBranchById(9999));
        Assert.assertFalse(TreeTest.tree.removeBranchById(null));
    }

    @Test
    public void testRemoveBranchByIdSuccess()
    {
        Assert.assertTrue(TreeTest.tree.nodeExits(11));
        Assert.assertTrue(TreeTest.tree.removeBranchById(11));
        Assert.assertFalse(TreeTest.tree.nodeExits(11));

        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(11));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(112));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(113));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(114));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(115));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(1111));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(0).contains(1112));

        Assert.assertTrue(TreeTest.tree.getDirectChildrenIds(0).contains(1));
        Assert.assertTrue(TreeTest.tree.getIndirectChildrenIds(0).contains(12));

        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(11));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(112));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(113));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(114));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(115));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(1111));
        Assert.assertFalse(TreeTest.tree.getDirectChildrenIds(1).contains(1112));

        Assert.assertFalse(TreeTest.tree.nodeExits(11));
        Assert.assertFalse(TreeTest.tree.nodeExits(112));
        Assert.assertFalse(TreeTest.tree.nodeExits(113));
        Assert.assertFalse(TreeTest.tree.nodeExits(114));
        Assert.assertFalse(TreeTest.tree.nodeExits(115));
        Assert.assertFalse(TreeTest.tree.nodeExits(1111));
        Assert.assertFalse(TreeTest.tree.nodeExits(1112));
    }

    @Test
    public void testRemoveAllBranches()
    {
        Assert.assertTrue(TreeTest.tree.removeBranchById(0));
        Assert.assertFalse(TreeTest.tree.nodeExits(0));
        Assert.assertFalse(TreeTest.tree.nodeExits(1));
        Assert.assertFalse(TreeTest.tree.nodeExits(11));
        Assert.assertFalse(TreeTest.tree.nodeExits(12));
        Assert.assertFalse(TreeTest.tree.nodeExits(13));
        Assert.assertFalse(TreeTest.tree.nodeExits(14));
        Assert.assertFalse(TreeTest.tree.nodeExits(15));
        Assert.assertFalse(TreeTest.tree.nodeExits(111));
        Assert.assertFalse(TreeTest.tree.nodeExits(112));
        Assert.assertFalse(TreeTest.tree.nodeExits(113));
        Assert.assertFalse(TreeTest.tree.nodeExits(114));
        Assert.assertFalse(TreeTest.tree.nodeExits(115));
        Assert.assertFalse(TreeTest.tree.nodeExits(1111));
        Assert.assertFalse(TreeTest.tree.nodeExits(1112));
        Assert.assertFalse(TreeTest.tree.nodeExits(2));
        Assert.assertFalse(TreeTest.tree.nodeExits(3));
        Assert.assertFalse(TreeTest.tree.nodeExits(4));
        Assert.assertFalse(TreeTest.tree.nodeExits(5));
        Assert.assertFalse(TreeTest.tree.nodeExits(51));
        Assert.assertFalse(TreeTest.tree.nodeExits(52));
        Assert.assertFalse(TreeTest.tree.nodeExits(53));
        Assert.assertFalse(TreeTest.tree.nodeExits(54));
        Assert.assertFalse(TreeTest.tree.nodeExits(55));
        Assert.assertFalse(TreeTest.tree.nodeExits(551));
        Assert.assertFalse(TreeTest.tree.nodeExits(552));
        Assert.assertFalse(TreeTest.tree.nodeExits(5551));
        Assert.assertFalse(TreeTest.tree.nodeExits(55551));
    }
}
