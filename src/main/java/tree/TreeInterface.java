package tree;

import java.util.List;

public interface TreeInterface
{
    public void build();
    public Integer getParentId(Integer id);
    public List<Integer> getDirectChildrenIds(Integer parentId);
    public List<Integer> getIndirectChildrenIds(Integer parentId);
    public Integer getTopParentId(Integer id);
    public List<Integer> getAncestorIds(Integer parentId, boolean includeRootId);
    public List<Integer> getAncestorIds(Integer parentId);
    public List<Integer> getParentAndDescendants(Integer id);
    public boolean addChildrenId(Integer parentId, Integer id);
    public boolean removeBranchById(Integer id);
}
